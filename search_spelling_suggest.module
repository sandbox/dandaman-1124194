<?php

/**
 * Insert spelling index words only after all nodes have been processed to avoid
 * duplicate queries.
 */
function search_spelling_shutdown($word = FALSE) {
  static $words = array();
  if ($word) {
    $words[$word] = TRUE;
  }
  else {
    foreach (array_keys($words) as $word) {
      @db_query("INSERT INTO {search_spellings} (word, characters) VALUES ('%s', %d)", $word, strlen($word));
    }
  }
}

//  http://www.merriampark.com/ld.htm#FLAVORS
function drupal_levenshtein($string1, $string2) {
  $table = array();
  $length1 = drupal_strlen($string1);
  $length2 = drupal_strlen($string2);
  
  // Early optimizations. If the strings are zero length, equal, or too long,
  // we return quick values.
  if ($length1 == 0) return $length2;
  if ($length2 == 0) return $length1;
  if ($string1 == $string2) return 0;
  if ($length1 > 255 || $length2 > 255) return -1;
  
  for ($i = 0; $i <= $length1; $i++) {
    $table[$i] = array();
    $table[$i][0] = $i;
  }
  for ($i = 0; $i <= $length2; $i++) {
    $table[0][$i] = $i;
  }
  for ($i = 1; $i <= $length1; $i++) {
    $char1 = drupal_substr($string1, $i-1, 1);
    for ($j = 1; $j <= $length2; $j++) {
      $char2 = drupal_substr($string2, $j-1, 1);
      $cost = $char1 == $char2 ? 0 : 1;
      $table[$i][$j] = min(
                            $table[$i-1][$j] + 1,
                            $table[$i][$j-1] + 1,
                            $table[$i-1][$j-1] + $cost
                          );
      if($i > 1 && $j > 1 &&
          (drupal_substr($string1, $i, 1) == $char2) &&
          ($char1 == drupal_substr($string1, $j, 1))) {
        // transposition
        $table[$i][$j] = min($table[$i][$j], $table[$i-2][$j-2] + $cost);
      }
    }
  }
  return $table[$length1][$length2];
}

/**
 * Get a statistically generated spelling suggestion for a word. The suggestions
 * are not guaranteed to be correctly spelled words, merely the word in your search
 * index that is most similar according to the Levenshtein algorithm.
 *
 * @param $word
 *   The word for which you wish to get a suggestion
 *
 * @return array
 *    The return array has keys 'word', the spelling suggestion, and 'score'.
 *    The lowest and best score should be 1. Higher scores indicate a lower
 *    probability that the suggestion is actually related to the original word.
 *
 */
function search_spelling_suggestion($word) {
  $length = strlen($word);
  $result = db_query("SELECT word FROM {search_spellings} WHERE characters > %d AND characters < %d", max(variable_get('minimum_word_size', 3), $length - 3), $length + 3);
  // Low scores are better.
  $lowest = 10;
  $closest = NULL;
  while ($spelling = db_fetch_object($result)) {
    // Calculate the distance between the input word and the current spelling.
    $score = drupal_levenshtein($word, $spelling->word);

    // Check for an exact match
    if ($score == 0) {
      // Continue on with the loop. We're looking for the closest spelling, not
      // for the word itself.
      continue;
    }

    // If this score is less than the previous lowest, OR if a suggestion
    // hasn't been found yet, set them.
    if ($score <= $lowest || is_null($closest)) {
        // Set the closest match, and lowest score
        $closest  = $spelling->word;
        $lowest = $score;
    }
  }
  return array('word' => $closest, 'score' => $lowest);
}
